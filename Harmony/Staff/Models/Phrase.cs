﻿using System.Collections.Generic;
using Staff.Enums;

namespace Staff.Models
{
    /// <summary>
    /// A phrase is a sequence of notes in a particular time signature and may be part of a song.
    /// </summary>
    public class Phrase
    {
        public int Id { get; set; }
        /// <summary>
        /// The name of the phrase.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The time signature of the phrase
        /// </summary>
        public TimeSignature TimeSignature { get; set; }
        /// <summary>
        /// The speed of the phrase in beats per minute (BPM).
        /// </summary>
        public int BPM { get; set; }
        /// <summary>
        /// The sequence of notes that constitutes the phrase.
        /// </summary>
        public List<Note> Notes { get; set; }
    }
}
