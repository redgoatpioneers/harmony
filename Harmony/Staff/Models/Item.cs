﻿using Staff.Enums;

namespace Staff.Models
{
    /// <summary>
    /// An item is any object that can be carried by a character.
    /// </summary>
    public class Item
    {
        public int Id { get; set; }
        /// <summary>
        /// The name of the item.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The cost of the item in gold.
        /// </summary>
        public int Cost { get; set; }
        /// <summary>
        /// The weight of the item in grams (g).
        /// </summary>
        public int Weight { get; set; }
        /// <summary>
        /// The rarity of the item.
        /// </summary>
        public Rarity Rarity { get; set; }
    }
}
