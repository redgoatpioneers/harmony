﻿using Staff.Enums;

namespace Staff.Models
{
    /// <summary>
    /// An instrument is an item the player can equip for a character in order to play music
    /// </summary>
    public class Instrument: Item
    {
        /// <summary>
        /// The maximum intensity of the notes this instrument can produce.
        /// </summary>
        public int Intensity { get; set; }
        /// <summary>
        /// The clarity of sound produced by the instrument.
        /// </summary>
        public int Clarity { get; set; }
        /// <summary>
        /// The likelihood the instrument will play the correct note at the correct pitch.
        /// </summary>
        public int Reliability { get; set; }
        /// <summary>
        /// The difficulty of learning the instrument.
        /// </summary>
        public int Difficulty { get; set; }
        /// <summary>
        /// The material the instrument is made of.
        /// </summary>
        public Material Material { get; set; }
        /// <summary>
        /// The frequency (Hz) of the lowest note produced by the instrument.
        /// </summary>
        public float LowestPitch { get; set; }
        /// <summary>
        /// The frequency (Hz) of the highest note produced by the instrument.
        /// </summary>
        public float HighestPitch { get; set; }
        /// <summary>
        /// The number of pitch half-steps this instrument is transposed by from concert pitch.
        /// </summary>
        public int Transposition { get; set; }
    }
}
