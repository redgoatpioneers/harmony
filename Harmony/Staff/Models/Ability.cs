﻿using System;

namespace Staff.Models
{
    public class Ability
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Experience { get; set; }
    }
}
