﻿namespace Staff.Enums
{
    public enum Rarity
    {
        Common,
        Uncommon,
        Rare,
        Legendary,
        Mythic
    }
}
