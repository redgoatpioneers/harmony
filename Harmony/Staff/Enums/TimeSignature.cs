﻿using System;

namespace Staff.Enums
{
    public enum TimeSignature
    {
        FourFour,
        ThreeFour,
        TwoFour,
        TwoTwo,
        ThreeEight,
        FiveFour,
        SevenFour,
        SevenEight,
        NineEight,
        TwelveEight
    }
}
