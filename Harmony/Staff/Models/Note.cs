﻿using System;

namespace Staff.Models
{
    /// <summary>
    /// A note is a single distinct sound in a phrase or song.
    /// </summary>
    public class Note
    {
        public int Id { get; set; }
        /// <summary>
        /// The pitch of the note (e.g. E, Ab, C#).
        /// </summary>
        public string Pitch { get; set; }
        /// <summary>
        /// The length of the note described as a fraction of a whole note (e.g. a quarter note is .25).
        /// </summary>
        public float Duration { get; set; }
        /// <summary>
        /// The loudness or softness of a note, on a scale from 1-100.
        /// </summary>
        public int Intensity { get; set; }
    }
}
