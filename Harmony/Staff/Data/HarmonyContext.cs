﻿using Staff.Models;
using Microsoft.EntityFrameworkCore;

namespace Staff.Data
{
    public class HarmonyContext : DbContext
    {
        public HarmonyContext(DbContextOptions<HarmonyContext> options) : base(options)
        {
        }

        public DbSet<Character> Characters { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Phrase> Phrases { get; set; }
        public DbSet<Piece> Pieces { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Character>().ToTable("Character");
            modelBuilder.Entity<Item>().ToTable("Item");
            modelBuilder.Entity<Phrase>().ToTable("Phrase");
            modelBuilder.Entity<Piece>().ToTable("Piece");
        }
    }
}
