﻿using System.Collections.Generic;
using Staff.Models;
using Staff.Enums;

namespace Staff.Assets
{
    /// <summary>
    /// All instruments available in the game.
    /// </summary>
    public static class Instruments
    {
        public static List<Instrument> AllInstruments
        {
            get
            {
                return new List<Instrument>
                {
                    new Instrument
                    {
                        Name = "Alto Saxophone",
                        Cost = 500,
                        Weight = 2190,
                        Rarity = Rarity.Uncommon,
                        Clarity = 80,
                        Difficulty = 45,
                        Intensity = 65,
                        Reliability = 75,
                        Material = Material.Brass,
                        LowestPitch = Pitch.Dflat3,
                        HighestPitch = Pitch.Aflat5,
                        Transposition = -3
                    },
                    new Instrument
                    {
                        Name = "Concert Ukulele",
                        Cost = 100,
                        Weight = 585,
                        Rarity = Rarity.Common,
                        Clarity = 60,
                        Difficulty = 35,
                        Intensity = 40,
                        Reliability = 60,
                        Material = Material.Wood,
                        LowestPitch = 262,
                        HighestPitch = 524,
                        Transposition = 0
                    },
                };
            }
        }
    }
}
