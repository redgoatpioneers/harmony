﻿using System.Collections.Generic;

namespace Staff.Models
{
    /// <summary>
    /// A character is an entity that is under the control of the player.
    /// </summary>
    public class Character
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int MaximumHP { get; set; }
        public int CurrentHP { get; set; }
        public int MaximumMP { get; set; }
        public int CurrentMP { get; set; }
        public List<Ability> Abilities { get; set; }
        public Instrument Instrument { get; set; }
        public List<Item> Items { get; set; }
    }
}
