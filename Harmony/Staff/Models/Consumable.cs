﻿using System;

namespace Staff.Models
{
    /// <summary>
    /// A consumbale is any item that can be used by a character.
    /// </summary>
    public class Consumable: Item
    {
        /// <summary>
        /// The number of times the item can be used before it is destroyed.
        /// </summary>
        public int Uses { get; set; }
        /// <summary>
        /// The number of HP this item restores if used on a character.
        /// </summary>
        public int Healing { get; set; }
        /// <summary>
        /// The number of HP this item reduces if used on an enemy.
        /// </summary>
        public int Damage { get; set; }
    }
}
