﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Staff.Data;
using System.Threading.Tasks;

namespace Clef.Controllers
{
    public class CharactersController : Controller
    {
        private readonly HarmonyContext _context;
        public CharactersController(HarmonyContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.Characters.ToListAsync());
        }
    }
}
