﻿using Microsoft.AspNetCore.Mvc;
using Staff.Data;

namespace Clef.Controllers
{
    public class HomeController : Controller
    {
        private readonly HarmonyContext _context;
        public HomeController(HarmonyContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
