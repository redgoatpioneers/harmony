﻿using Staff.Models;
using Staff.Assets;
using System.Collections.Generic;
using System.Linq;

namespace Staff.Data
{
    public static class DbInitializer
    {
        public static void Initialize(HarmonyContext context)
        {
            context.Database.EnsureCreated();

            if (context.Characters.Any())
                return;

            var characters = new List<Character>
            {
                new Character
                {
                    Name = "Imp y Celyn",
                    Level = 1,
                    Experience = 0,
                    MaximumHP = 100,
                    CurrentHP = 100,
                    MaximumMP = 20,
                    CurrentMP = 20,
                    Abilities = new List<Ability>(),
                    Instrument = null,
                    Items = new List<Item>()
                }
            };
            foreach (Character character in characters)
                context.Characters.Add(character);
            context.SaveChanges();

            foreach (Instrument instrument in Instruments.AllInstruments)
                context.Items.Add(instrument);
            context.SaveChanges();
        }
    }
}
