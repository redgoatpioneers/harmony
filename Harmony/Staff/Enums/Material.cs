﻿namespace Staff.Enums
{
    public enum Material
    {
        Paper,
        Plastic,
        Wood,
        Brass,
        Glass,
        Stone
    }
}
