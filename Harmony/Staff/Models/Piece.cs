﻿using System.Collections.Generic;

namespace Staff.Models
{
    /// <summary>
    /// A piece is a sequence of one or more phrases.
    /// </summary>
    public class Piece
    {
        public int Id { get; set; }
        /// <summary>
        /// The name of the piece.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The sequence of notes that constitutes the phrase.
        /// </summary>
        public List<Phrase> Phrases { get; set; }
    }
}
